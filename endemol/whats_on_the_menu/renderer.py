"""
    Version: To be filled as per the version to be released

    client_name.template.renderer
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Implements the data renderer of the game

    Define additional renderering functions required by your engine.
    These are likely to be for bonus wins, non-standard parts of the definition, etc.
"""
from . import game_version
from collections import OrderedDict


class TemplateRenderer(object):
    """Define a class with extension / override methods to modify OGLE_SlotRendererMixin and mix
    this in with your engine class like so:
    class MyEngine(TemplateRenderer, OGLE_Slot):
        ...
    """

    def get_definition(self):
        """ Game init rendered response.

        :return: Rendered definition for the game along with version no.
        """
        definition = super(TemplateRenderer, self).get_definition()

        # version of game
        definition['gameVersion'] = game_version

        return definition

    def get_gamestate(self):
        """
        Game renderer
        :returns: game state object
        """
        gamestate = super(TemplateRenderer, self).get_gamestate()
        state = self.value['gamestate'].result

        # current version of game
        gamestate['gameVersion'] = game_version

        if state.get('triggering_state'):
            # Added triggering state details in gamestate
            triggering_details = OrderedDict()
            triggering_state = state['triggering_state']
            triggering_details['reelSetIndex'] = triggering_state['reel_set_index']
            triggering_details['stopList'] = triggering_state['stop_list']
            triggering_details['view'] = [map(str, row) for row in triggering_state['symbol_grid']]
            triggering_details['stopList'] = triggering_state['stop_list']
            triggering_details['currentWinnings'] = triggering_state['current_winnings']
            gamestate['triggeringDetails'] = triggering_details

        if state.get('freespin_state'):
            # Added freespin state details in gamestate
            freespin_state = state['freespin_state']
            freegame_state = OrderedDict()
            freegame_state['numFreeSpins'] = freespin_state['num_free_spins']
            freegame_state['currentPlay'] = freespin_state['current_play']
            freegame_state['freespinWinnings'] = freespin_state['freespin_winnings']

        return gamestate

    @classmethod
    def get_manual_finish_result(self):
        game_state = {}

        return game_state
