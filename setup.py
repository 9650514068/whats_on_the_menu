from setuptools import setup, find_packages
from codecs import open
from os import path
import re

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# set version from README.md, expected line of form:
# > version - n.n.n
version_re = re.compile('>\s+version\s+-\s+([0-9]+\.[0-9]+\.[0-9]+)')
m = version_re.search(long_description)

if m is None or len(m.groups()) != 1:
    raise Exception("Unable to determine version number from README.md")

version = m.groups()[0]

setup(
    name='template-client_name',
    version=version,

    description='template Slot Engine for OGA',
    long_description=long_description,

    url='https://www.playzido.com/games/',
    author='Playzido',
    author_email='info@playzido.com',

    license='proprietary',

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Topic :: Games/Entertainment',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 2.7',
    ],

    packages=find_packages(),

    install_requires=[
        'OGA',
    ],
)